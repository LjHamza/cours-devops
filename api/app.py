from flask import Flask, jsonify, request, make_response
import sqlalchemy as db
import jwt 
import hashlib
from functools import wraps

jwt_secret = "secret"
app = Flask(__name__)
app.config['SECRET_KEY'] = "Password123!"

engine = db.create_engine("sqlite:///database.sqlite", connect_args={'check_same_thread': False})
conn = engine.connect()
metadata = db.MetaData()

def check_logged():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            token_header = request.cookies.get("access_token")
            if not token_header or not jwt.decode(token_header, jwt_secret, algorithm="HS256")["user"]:
                return jsonify(msg="Logged users only!"), 403
            return fn(*args, **kwargs)
        return decorator
    return wrapper

def hashpass(passw):
    return hashlib.md5(passw.encode()).hexdigest()

User = db.Table("User", metadata,
                db.Column("name", db.String(255), unique=True),
                db.Column("password", db.String(80)))


# Seed
try:
    metadata.create_all(engine)
    conn.execute(db.insert(User).values(name='Admin', password=hashpass("admin")))
except:
    pass

@app.route('/api/users', methods=['GET'])
@check_logged()
def get_users():
    return jsonify([dict(user) for user in conn.execute(User.select()).all()])

@app.route('/api/register', methods=['POST'])
def register():
    data = request.json
    conn.execute(db.insert(User).values(name=data["username"], password=hashpass(data["password"])))
    return jsonify({"message": "OK"})

@app.route('/api/login', methods=['POST'])
def login():
    data = request.json
    user = conn.execute(User.select(username=data["username"], password=hashpass(data["password"]))).all()
    if len(user) == 0:
        return jsonify({"message": "Invalid credentials"}), 401
    response = make_response(jsonify({"message": "OK"}))
    response.set_cookie('access_token', jwt.encode({"user": data["username"]}, jwt_secret, algorithm="HS256"))
    return response
